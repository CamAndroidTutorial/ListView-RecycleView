package com.bunhann.listviewrecycleview_tutorial.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bunhann.listviewrecycleview_tutorial.R;

public class AlphaAdapter extends BaseAdapter {

    Context context;
    String title[];
    String description[];

    LayoutInflater inflater;

    public AlphaAdapter(Context context, String[] title, String[] description) {
        this.context = context;
        this.title = title;
        this.description = description;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_custom2, null);
        }

        TextView tvMain = (TextView) convertView.findViewById(R.id.tvMain);
        TextView tvSub = (TextView) convertView.findViewById(R.id.tvSub);
        ImageView icon = (ImageView) convertView.findViewById(R.id.imgIcon);
        tvMain.setText(title[position]);
        tvSub.setText(description[position]);
        if (position%2==0){
            icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cloud));
        } else if (position%3==0){
            icon.setImageResource(R.drawable.ic_account_balance);
        } else
            icon.setImageResource(R.drawable.ic_barcode);
        return convertView;
    }
}
