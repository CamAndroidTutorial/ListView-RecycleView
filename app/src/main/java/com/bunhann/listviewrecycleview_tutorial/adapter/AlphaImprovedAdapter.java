package com.bunhann.listviewrecycleview_tutorial.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bunhann.listviewrecycleview_tutorial.R;

public class AlphaImprovedAdapter extends BaseAdapter {

    Context context;
    String title[];
    String description[];

    LayoutInflater inflater;

    private static class ViewHolder {

        TextView main;
        TextView sub;
        ImageView icon;

    }

    public AlphaImprovedAdapter(Context context, String[] title, String[] description) {
        this.context = context;
        this.title = title;
        this.description = description;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Check if an existing view is being reused, otherwise inflate the view

        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {

            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_custom2, null);

            viewHolder.main = (TextView) convertView.findViewById(R.id.tvMain);
            viewHolder.sub = (TextView) convertView.findViewById(R.id.tvSub);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.imgIcon);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.main.setText(title[position]);
        viewHolder.sub.setText(description[position]);
        if (position%2==0){
            viewHolder.icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cloud));
        } else if (position%3==0){
            viewHolder.icon.setImageResource(R.drawable.ic_account_balance);
        } else
            viewHolder.icon.setImageResource(R.drawable.ic_barcode);

        // Return the completed view to render on screen
        return convertView;
    }
}
