package com.bunhann.listviewrecycleview_tutorial.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AlphabetAdapter extends BaseAdapter{
    Context context;
    String title[];
    String description[];

    public AlphabetAdapter(Context context, String[] title, String[] description) {
        this.context = context;
        this.title = title;
        this.description = description;
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater =  LayoutInflater.from(context);
        convertView = inflater.inflate(android.R.layout.simple_list_item_2, null);

        TextView textViewtitle = (TextView) convertView.findViewById(android.R.id.text1);
        TextView textViewdescription = (TextView) convertView.findViewById(android.R.id.text2);

        textViewtitle.setText(title[position]);
        textViewdescription.setText(description[position]);

        return convertView;
    }
}
