package com.bunhann.listviewrecycleview_tutorial.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bunhann.listviewrecycleview_tutorial.R;
import com.bunhann.listviewrecycleview_tutorial.models.User;

import java.util.List;

public class UserAdapter extends ArrayAdapter<User> {

    public UserAdapter(@NonNull Context context, @NonNull List<User> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        User user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cus_list, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvPassword = (TextView) convertView.findViewById(R.id.tvPassword);
        // Populate the data into the template view using the data object
        tvName.setText(user.getUsername());
        tvPassword.setText(user.getPassword());
        // Return the completed view to render on screen
        return convertView;
    }
}
