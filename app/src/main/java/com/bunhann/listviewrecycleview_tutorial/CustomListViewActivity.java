package com.bunhann.listviewrecycleview_tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bunhann.listviewrecycleview_tutorial.adapter.UserAdapter;
import com.bunhann.listviewrecycleview_tutorial.models.User;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewActivity extends AppCompatActivity {

    private ListView lvUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        Faker faker = new Faker();
        final List<User> userList = new ArrayList<User>();
        userList.add(new User("John", "123"));
        for (int i=0; i<30;i++){
            userList.add(new User(faker.name().fullName(),faker.name().lastName()));
        }

        UserAdapter userAdapter = new UserAdapter(this, userList);
        lvUser = (ListView) findViewById(R.id.lsAlphabet);

        lvUser.setAdapter(userAdapter);
        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(CustomListViewActivity.this, userList.get(position).toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
