package com.bunhann.listviewrecycleview_tutorial.recycleadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bunhann.listviewrecycleview_tutorial.R;
import com.bunhann.listviewrecycleview_tutorial.models.Movie;

import java.util.ArrayList;

public class MovieRecycleAdapter extends RecyclerView.Adapter<MovieRecycleAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Movie> movies;
    private LayoutInflater inflater;

    public MovieRecycleAdapter(Context context, ArrayList<Movie> movies) {
        this.context = context;
        this.movies = movies;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View convertView = inflater.inflate(R.layout.list_movie_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(convertView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Movie movie = movies.get(position);

        holder.tvTitle.setText(movie.getTitle());
        holder.tvGenre.setText(movie.getGenre());
        holder.tvReleaseYear.setText(String.valueOf(movie.getReleaseYear()));

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTitle, tvGenre, tvReleaseYear;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvGenre = (TextView) itemView.findViewById(R.id.tvGenre);
            tvReleaseYear = (TextView) itemView.findViewById(R.id.tvReleaseYear);

        }
    }
}
