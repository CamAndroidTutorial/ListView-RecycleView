package com.bunhann.listviewrecycleview_tutorial.recycleadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bunhann.listviewrecycleview_tutorial.R;
import com.bunhann.listviewrecycleview_tutorial.models.User;

import java.util.List;

public class UserRecycleAdapter extends RecyclerView.Adapter<UserRecycleAdapter.ViewHolder> {

    private Context context;
    private List<User> userList;
    private LayoutInflater inflater;

    // ... constructor and member variables
    public UserRecycleAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
        inflater = LayoutInflater.from(context);
    }


    // Usually involves inflating a layout from XML and returning the holder
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Inflate the custom layout
        View convertView = inflater.inflate(R.layout.list_recycle_item1, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(convertView);

        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // Get the data model based on position
        User user = userList.get(position);

        // Set item views based on your views and data model
        TextView tvRecycleName = holder.tvName;
        tvRecycleName.setText(user.getUsername());

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvRecycleName);
        }
    }
}
