package com.bunhann.listviewrecycleview_tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bunhann.listviewrecycleview_tutorial.models.User;
import com.bunhann.listviewrecycleview_tutorial.recycleadapter.UserRecycleAdapter;
import com.github.javafaker.Faker;

import java.util.ArrayList;

public class RecycleViewActivity extends AppCompatActivity {

    private RecyclerView rvData;
    private ArrayList<User> users;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycleview);

        rvData = (RecyclerView) findViewById(R.id.rvData);

        Faker faker = new Faker();
        users = new ArrayList<>();
        for(int i =0;i<20;i++){
            users.add(new User(faker.name().fullName(), faker.gameOfThrones().city()));
        }
        // Create adapter passing in the sample user data
        UserRecycleAdapter userRecycleAdapter = new UserRecycleAdapter(this, users);

        rvData.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // Attach the adapter to the recyclerview to populate items
        rvData.setAdapter(userRecycleAdapter);
        // Set layout manager to position the items
        rvData.setLayoutManager(new LinearLayoutManager(this));

    }
}
