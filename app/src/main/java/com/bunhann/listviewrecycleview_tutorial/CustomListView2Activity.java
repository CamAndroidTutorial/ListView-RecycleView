package com.bunhann.listviewrecycleview_tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.bunhann.listviewrecycleview_tutorial.adapter.AlphaImprovedAdapter;
import com.github.javafaker.Faker;

public class CustomListView2Activity extends AppCompatActivity {

    private ListView lvData;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        lvData = (ListView) findViewById(R.id.lsAlphabet);
        Faker faker = new Faker();
        final String main[] = new String[50];
        final String sub[] = new String[50];
        for (int i=0; i<50; i++){
            main[i] = faker.artist().name();
            sub[i] = faker.pokemon().name();
        }

        //AlphaAdapter adapter = new AlphaAdapter(this, main, sub);
        AlphaImprovedAdapter adapter = new AlphaImprovedAdapter(this, main, sub);
        lvData.setAdapter(adapter);

    }
}
