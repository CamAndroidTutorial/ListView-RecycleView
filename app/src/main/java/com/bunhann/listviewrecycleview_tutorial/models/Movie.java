package com.bunhann.listviewrecycleview_tutorial.models;

import com.github.javafaker.Faker;

import java.util.ArrayList;

public class Movie {

    private String title;
    private String genre;
    private int releaseYear;
    private boolean dvdAvailable;

    public Movie() {
    }

    public Movie(String title, String genre, int releaseYear, boolean dvdAvailable) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.dvdAvailable = dvdAvailable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public boolean isDvdAvailable() {
        return dvdAvailable;
    }

    public void setDvdAvailable(boolean dvdAvailable) {
        this.dvdAvailable = dvdAvailable;
    }

    public static ArrayList<Movie> createMovieList(int numMovies){

        ArrayList<Movie> movies = new ArrayList<>();
        Faker faker = new Faker();
        for (int i=1;i<=numMovies;i++){
            movies.add(new Movie(faker.gameOfThrones().character(), faker.internet().emailAddress(), faker.number().numberBetween(1990, 2018), faker.bool().bool()));
        }

        return movies;
    }
}
