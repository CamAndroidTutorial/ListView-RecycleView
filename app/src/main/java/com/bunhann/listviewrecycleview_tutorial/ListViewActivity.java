package com.bunhann.listviewrecycleview_tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListViewActivity extends AppCompatActivity {

    private ListView lsAlphabet;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        final String engAlpha[] = getResources().getStringArray(R.array.eng_alphabet);

        lsAlphabet = (ListView) findViewById(R.id.lsAlphabet);
        ArrayAdapter<String> alphabetAdapter = new ArrayAdapter<String>(this,
               android.R.layout.simple_list_item_1, engAlpha);

        lsAlphabet.setAdapter(alphabetAdapter);
        lsAlphabet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListViewActivity.this, engAlpha[position], Toast.LENGTH_SHORT).show();
            }
        });
    }
}
