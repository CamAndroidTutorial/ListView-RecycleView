package com.bunhann.listviewrecycleview_tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private Button btnListView, btnListView2, btnCustomListView, btnCustomListView2, btnRecycleView, btnRecycleView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        btnListView = (Button) findViewById(R.id.btnListView);
        btnListView2 = (Button) findViewById(R.id.btnListView2);
        btnCustomListView = (Button) findViewById(R.id.btnCustomListView);
        btnCustomListView2 = (Button) findViewById(R.id.btnCustomListview2);

        btnRecycleView = (Button) findViewById(R.id.btnRecycleView);
        btnRecycleView2 = (Button) findViewById(R.id.btnRecycleView2);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        btnListView.setOnClickListener(this);
        btnListView2.setOnClickListener(this);
        btnCustomListView.setOnClickListener(this);
        btnCustomListView2.setOnClickListener(this);
        btnRecycleView.setOnClickListener(this);
        btnRecycleView2.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        Intent intent;
        switch (btnId){
            case R.id.btnListView:
                intent = new Intent(v.getContext(), ListViewActivity.class);
                startActivity(intent);
                break;

            case R.id.btnListView2:
                intent = new Intent(v.getContext(), ListView2Activity.class);
                startActivity(intent);
                break;

            case R.id.btnCustomListView:
                intent = new Intent(v.getContext(), CustomListViewActivity.class);
                startActivity(intent);
                break;

            case R.id.btnCustomListview2:
                intent = new Intent(v.getContext(), CustomListView2Activity.class);
                startActivity(intent);
                break;
            case R.id.btnRecycleView:
                intent = new Intent(v.getContext(), RecycleViewActivity.class);
                startActivity(intent);
                break;
            case R.id.btnRecycleView2:
                intent = new Intent(v.getContext(), MovieRecycleViewActivity.class);
                startActivity(intent);
                break;
        }

    }
}
