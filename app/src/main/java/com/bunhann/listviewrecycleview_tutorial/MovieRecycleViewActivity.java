package com.bunhann.listviewrecycleview_tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.bunhann.listviewrecycleview_tutorial.models.Movie;
import com.bunhann.listviewrecycleview_tutorial.recycleadapter.MovieRecycleAdapter;
import com.bunhann.listviewrecycleview_tutorial.recycleenhancement.RecyclerTouchListener;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;

public class MovieRecycleViewActivity extends AppCompatActivity {

    private RecyclerView rvMovie;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recycleview);

        rvMovie = (RecyclerView) findViewById(R.id.rvData);

        final ArrayList<Movie> movies = Movie.createMovieList(100);

        MovieRecycleAdapter movieRecycleAdapter = new MovieRecycleAdapter(this, movies);

        rvMovie.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        //rvMovie.addItemDecoration(new CustomItemDecorator(this, LinearLayoutManager.VERTICAL, 16));

        //rvMovie.setItemAnimator(new DefaultItemAnimator());

        rvMovie.setAdapter(movieRecycleAdapter);
        rvMovie.setLayoutManager(new LinearLayoutManager(this));

        rvMovie.setItemAnimator(new SlideInLeftAnimator());

        rvMovie.addOnItemTouchListener(new RecyclerTouchListener(this, rvMovie, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Movie m = movies.get(position);
                Toast.makeText(MovieRecycleViewActivity.this, m.getTitle(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
