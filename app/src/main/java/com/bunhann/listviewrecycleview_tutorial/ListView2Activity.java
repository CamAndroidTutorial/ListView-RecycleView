package com.bunhann.listviewrecycleview_tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bunhann.listviewrecycleview_tutorial.adapter.AlphabetAdapter;

public class ListView2Activity extends AppCompatActivity {

    private ListView lsAlphabet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);


        final String engAlpha[] = getResources().getStringArray(R.array.eng_alphabet);
        final String lowerAlpha[] = getResources().getStringArray(R.array.lower_alpha);


        lsAlphabet = (ListView) findViewById(R.id.lsAlphabet);
        AlphabetAdapter alphabetAdapter = new AlphabetAdapter(this, engAlpha, lowerAlpha);
        lsAlphabet.setAdapter(alphabetAdapter);
        lsAlphabet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListView2Activity.this, engAlpha[position], Toast.LENGTH_SHORT).show();
            }
        });

    }
}
